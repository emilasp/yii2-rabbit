Модуль Rabbit для Yii2
=============================

Модуль для работы с сервером очередей RabbitMQ

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-rabbit": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-rabbit.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'rabbit' => []
    ],
```
